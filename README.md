# Mockserver Express

The purpose of this tool is to make a custom response locally. Sometimes we just don't want to put any example response online.

## How to run

```bash
$ npm start
```
