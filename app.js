const express = require('express')
const app = express()
const port = 3000

const products = require('./routes/products')
app.use('/v1/products', products)

const users = require('./routes/users')
app.use('/v1/users', users)

app.listen(port, () => console.log(`Example app listening at http://localhost:${port}`))