module.exports = function(req, res, response) {
	if (req.headers['x-application'] == 'ABC123') {
		res.send(JSON.stringify(response))
	} else {
		res.send(JSON.stringify({
			"success" : false,
			"data" : null,
			"errors" : 400
		}))
	}
}