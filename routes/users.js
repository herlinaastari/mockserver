const express = require('express')
const router = express.Router()
const validateHeaders = require('../headers')

router.get('/', function (req, res) {
  const response = require('../responses/users/users.json')
  const validResponse = validateHeaders(req, res, response)
  res.send(JSON.stringify(validResponse))
})

router.get('/1', function (req, res) {
  const response = require('../responses/users/user_1.json')
  const validResponse = validateHeaders(req, res, response)
  res.send(JSON.stringify(validResponse))
})

router.post('/', function (req, res) {
  const response = require('../responses/users/user_create_4.json')
  const validResponse = validateHeaders(req, res, response)
  res.send(JSON.stringify(validResponse))
})

router.put('/1', function (req, res) {
  const response = require('../responses/users/user_update_1.json')
  const validResponse = validateHeaders(req, res, response)
  res.send(JSON.stringify(validResponse))
})

module.exports = router