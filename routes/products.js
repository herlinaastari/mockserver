const express = require('express')
const router = express.Router()

const productsResponse = require('../responses/products/products.json')
router.get('/', function (req, res) {
  res.json(productsResponse)
})

const product1Response = require('../responses/products/product_1.json')
router.get('/1', function (req, res) {
  res.json(product1Response)
})

module.exports = router